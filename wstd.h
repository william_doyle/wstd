#pragma once

#include <memory>
#include <time.h>



namespace wstd {



	template<class T>
	void print(T message){
		std::cout << message << std::endl;
	};


	template <class T>
	struct binary_tree_node {
		std::shared_ptr<binary_tree_node<T>> left_;
		std::shared_ptr<binary_tree_node<T>> right_;
		T data_;

		binary_tree_node(T val = NULL, std::shared_ptr<binary_tree_node<T>> l_ = nullptr, std::shared_ptr<binary_tree_node<T>> r_ = nullptr) {
			binary_tree_node::data_ = val;
			binary_tree_node::left_ = l_;
			binary_tree_node::right_ = r_;
		}
	};



	template <class T>
	class binary_tree{
	public:
		binary_tree() :root_(nullptr) {}
		void insert(T);
		void print();
		std::shared_ptr<binary_tree_node<T>> find(T);
	private:
		std::shared_ptr<binary_tree_node<T>> root_;
		void insert(T, std::shared_ptr<binary_tree_node<T>>&);
		void inorder(std::shared_ptr<binary_tree_node<T>> root);
		std::shared_ptr<binary_tree_node<T>> find(T, std::shared_ptr<binary_tree_node<T>>&);
	};



}