#pragma once
#include <iostream>
#include "wstd.h"

namespace wstd {

	/**
	 * 
	 * insert 
	 * takes one argument
	 * 
	 * */
	template <class T>
	void binary_tree<T>::insert(T value) {
		insert(value, root_);
	}

	
	template <class T>
	void binary_tree<T>::insert(T value, std::shared_ptr<binary_tree_node<T>>& leaf) {

		if (leaf == nullptr) {
			leaf = std::make_shared<binary_tree_node<T>>(value);
			return;
		}//end if

		if (value < leaf->data_)
			insert(value, leaf->left_);
		else if (value > leaf->data_)
			insert(value, leaf->right_);
	}

	template <class T>
	void binary_tree<T>::inorder(std::shared_ptr<binary_tree_node<T>> root) {

		if (root == nullptr) return;				/* if this node is null its the last node in the branch. so quit */

		inorder(root->left_);
		std::cout << root->data_ << " ";			/* output */
		inorder(root->right_);

		return;
	}

	template <class T>
	void binary_tree<T>::print() {
		inorder(root_);
	}


	template <class T>
	std::shared_ptr<binary_tree_node<T>> binary_tree<T>::find(T v) {
		return find(v, binary_tree::root_);
	}

	template <class T>
	std::shared_ptr<binary_tree_node<T>> binary_tree<T>::find(T value, std::shared_ptr<binary_tree_node<T>>& start_point) {

		//write the algorithum yourself

		if (start_point == nullptr)
			return nullptr;

		if (value == start_point->data_)
			return start_point;

		else if (value > start_point->data_)
			return find(value, start_point->right_);

		else if (value < start_point->data_)
			return find(value, start_point->left_);

		else {
			std::cout << "Value cannot be found in the tree\n";
			return nullptr;
		}



	}
}//close namespace