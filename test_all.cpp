//#pragma once
#include "trees.cpp"
#include <ctime>

#define DEMO_DOUBLE 69.99
#define DEMO_INT 69
#define NUMBER_OF_TIMES_TO_ADD_RANDOM_VALUES_TO_TREE 1000000


int main() {


	{/* enclose the binary_tree part of the test in its own scope */
		srand(unsigned(time(NULL)));																		/* Seed the random number generator with current time */

		std::shared_ptr<wstd::binary_tree<int>> the_binary_tree = std::make_shared<wstd::binary_tree<int>>();				/* Make a shared pointer to a new instance of the BinaryTree class */

		for (int i = 0; i < NUMBER_OF_TIMES_TO_ADD_RANDOM_VALUES_TO_TREE; i++)								/* run the following line of code the specified amount of times	*/
			the_binary_tree->insert(rand() % 100);															/* insert a random value between 0 and 100 into the binary tree */

		the_binary_tree->insert(DEMO_INT);																	/* insert a predefined interger value into the binary tree */

		the_binary_tree->print();																			/* output the binary tree to standard out */

		//delete *b;  //no need for memory managment when you use shared pointers for everything 

		std::cout << "\nSearching for DEMO_INT which has a value of " << DEMO_INT << ".\n";					/* output a message to the standard out to tell the user we will search the tree for the pre defined value */

		std::shared_ptr<wstd::binary_tree_node<int>> temp__ = the_binary_tree->find(DEMO_INT);

		std::cout << "Pointer:\t" << temp__ << "\n";
		std::cout << "Value:  \t" << temp__->data_ << "\n";

		std::cout << std::endl << std::endl << "NOW DO THE SAME THING WITH DOUBLES" << std::endl;
		/***********************************************************************************************/

		srand(unsigned(time(NULL)));
		std::shared_ptr<wstd::binary_tree<double>> double_the_binary_tree = std::make_shared<wstd::binary_tree<double>>();

		for (int i = 0; i < NUMBER_OF_TIMES_TO_ADD_RANDOM_VALUES_TO_TREE; i++)
			double_the_binary_tree->insert(double(rand() % 100) + 0.67);

		double_the_binary_tree->insert(DEMO_DOUBLE);
		double_the_binary_tree->print();

		std::cout << "\nSearching for DEMO_DOUBLE which has a value of " << DEMO_DOUBLE << ".\n";
		std::shared_ptr<wstd::binary_tree_node<double>> double_temp__ = double_the_binary_tree->find(DEMO_DOUBLE);

		std::cout << "DOUBLE Pointer:\t" << double_temp__ << "\n";
		std::cout << "DOUBLE Value:  \t" << double_temp__->data_ << "\n";

		/***********************************************************************************************/


	}

	{
		wstd::print("This is what I have to say");
	}

	{
		wstd::print("CURRENT TIME: ") ;
		wstd::print(wstd::current_time);
	}

	return 1;
}